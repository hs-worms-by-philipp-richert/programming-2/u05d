// Aufgabe u05d
// Dreiecksrechnung mit Aggregation und Freundfunktionen
// Philipp Richert
// 21.11.2022

#include <iostream>
#include "cDreieck.h"

using namespace std;

int main() {
	cDreieck rects[3] = { {{14.3, 2.41}, {1.24, 16.3}, {4.73, 6.42}},
						  {{0.5, 1.0}, {1.5, 0.0}, {0.5, 0.0}} };

	for (cDreieck item : rects) {
		item.ausgabe();
	}

	cout << "Vergleich der ersten beiden Dreiecke: " << dreieckVergleich(rects[0], rects[1]) << endl
		<< "Vergleich der letzten beiden Dreiecke: " << dreieckVergleich(rects[1], rects[2]) << endl;

	return 0;
}