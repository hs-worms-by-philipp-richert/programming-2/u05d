#include "cPunkt.h"

cPunkt::cPunkt(double x_in, double y_in) {
	x = max(-10.0, min(10.0, x_in));
	y = max(-10.0, min(10.0, y_in));
}

void cPunkt::ausgabe() {
	cout << "Koordinaten: x = " << x << "; y = " << y << endl;
}

double cPunkt::getX() {
	return x;
}

double cPunkt::getY() {
	return y;
}