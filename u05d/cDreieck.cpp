#include "cDreieck.h"

cDreieck::cDreieck(cPunkt p1_in, cPunkt p2_in, cPunkt p3_in) : p1(p1_in), p2(p2_in), p3(p3_in) {
}

double cDreieck::umfangD() {
	double mantle = distanceP(p1, p2) + distanceP(p1, p3) + distanceP(p2, p3);
	return mantle;
}

double cDreieck::distanceP(cPunkt p1_in, cPunkt p2_in) {
	double p1p2 = sqrt(pow(p2_in.getX() - p1_in.getX(), 2) + pow(p2_in.getY() - p1_in.getY(), 2));
	return p1p2;
}

double cDreieck::flaecheD() {
	double s = umfangD() / 2;
	double f = sqrt(s * (s - distanceP(p1, p2)) * (s - distanceP(p1, p3)) * (s - distanceP(p2, p3)));

	return f;
}

void cDreieck::ausgabe() {
	cout << "Dreieck:" << endl
		<< "\tA ";
	p1.ausgabe();
	cout << "\tB ";
	p2.ausgabe();
	cout << "\tC ";
	p3.ausgabe();

	cout << "\tUmfang: " << umfangD() << endl
		<< "\tFlaeche: " << flaecheD() << endl << endl;
}

int dreieckVergleich(cDreieck d1, cDreieck d2) {
	double f1 = d1.flaecheD();
	double f2 = d2.flaecheD();

	if (f1 > f2) return 1;
	else if (f1 == f2) return 0;
	else if (f1 < f2) return -1;
}