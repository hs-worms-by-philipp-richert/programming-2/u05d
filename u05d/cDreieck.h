#pragma once

#include <cmath>
#include <iostream>
#include "cPunkt.h"

using namespace std;

class cDreieck
{
private:
	cPunkt p1;
	cPunkt p2;
	cPunkt p3;
	double umfangD();
	double distanceP(cPunkt, cPunkt);
public:
	cDreieck(cPunkt = cPunkt(0.0, 1.0), cPunkt = cPunkt(1.0, 0.0), cPunkt = cPunkt(0.0, 0.0));
	double flaecheD();
	void ausgabe();
	friend int dreieckVergleich(cDreieck d1, cDreieck d2);
};

