#pragma once

#include <algorithm>
#include <iostream>

using namespace std;

class cPunkt
{
private:
	double x;
	double y;
public:
	cPunkt(double = 0.0, double = 0.0);
	void ausgabe();
	double getX();
	double getY();
};

